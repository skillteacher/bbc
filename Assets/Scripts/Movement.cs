using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    private float speed = 10f;
    private float jumpForce = 10f;
    private Rigidbody2D playerRigidbody;

    private void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float jumpInput = Input.GetAxis("Jump");
        Move(horizontalInput);
        Jump(jumpInput);

    }

    private void Move(float direction)
    {
        Vector2 velocity = new Vector2(speed * direction,playerRigidbody.velocity.y);
        playerRigidbody.velocity = velocity;
    }
    private void Jump(float jumpInput)
    {
        Vector2 velocity = new Vector2(playerRigidbody.velocity.x, jumpForce * jumpInput);
        playerRigidbody.velocity = velocity;
    }
}
